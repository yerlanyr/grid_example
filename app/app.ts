import {WidgetService, widgetServiceName} from './widgetsService';
import states from './states';
import * as Grid from './grid';
import * as WidgetBase from './widget.base';
import * as Widget1 from './widgets/widget1';
import * as Widget2 from './widgets/widget2';
import * as Widget3 from './widgets/widget3';
import * as Widget4 from './widgets/widget4';
declare var angular: ng.IAngularStatic;
var app = angular.module('app', ['ui.router', 'angular-bind-html-compile'])
    .service(widgetServiceName, WidgetService)
    .component(Grid.name, Grid.component)
    .component(WidgetBase.name, WidgetBase.component)
    .component(Widget1.name, Widget1.component)
    .component(Widget2.name, Widget2.component)
    .component(Widget3.name, Widget3.component)
    .component(Widget4.name, Widget4.component)
    .config(states)
    .name;