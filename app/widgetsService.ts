export class WidgetService{
    static $inject = ['$q'];
    constructor(private $q: ng.IQService){}
    widgets = {
        page1: {
            rows: 2,
            cols: 2,
            widgets: [
                {
                    component: 'widget1',
                    row: 0,
                    col: 0
                },
                {
                    component: 'widget2',
                    row: 0,
                    col: 1
                },
                {
                    component: 'widget3',
                    row: 1,
                    col: 0
                },
                {
                    component: 'widget4',
                    row: 1,
                    col: 1
                }
            ]

        }
    };
    getWidgets(name){
        var deferred = this.$q.defer();
        deferred.resolve(this.widgets[name]);
        return deferred.promise;
    }
}
export const widgetServiceName = 'geWidgetService';