import {WidgetService, widgetServiceName} from './widgetsService';

export default function($stateProvider: ng.ui.IStateProvider) {
    var mainState: ng.ui.IState = {
        name: 'main',
        url:'',
        template: `
        <ul>
        <li><a ui-sref="page1">Widgets page 1</a></li>
        </ul>
        `
    }; 
    var widgetsState = {
        name: 'widgets',
        url: '/widgets',
        abstract: false,
        template: `<div ui-view='content'></div>`
    };
    var page1: ng.ui.IState = {
        name: 'page1',
        url: '/page1',
        parent: 'widgets',
        resolve: {
            widgets: function(geWidgetService: WidgetService){
                return geWidgetService.getWidgets('page1');
            }
        },
        views: {
            'content@widgets': {
                template: '<grid grid-info="$resolve.widgets"></grid>'
            }
        }
    };
    $stateProvider.state(widgetsState);
    $stateProvider.state(mainState);
    $stateProvider.state(page1);
}