/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function default_1($stateProvider) {
    var mainState = {
        name: 'main',
        url: '',
        template: "\n        <ul>\n        <li><a ui-sref=\"page1\">Widgets page 1</a></li>\n        </ul>\n        "
    };
    var widgetsState = {
        name: 'widgets',
        url: '/widgets',
        abstract: false,
        template: "<div ui-view='content'></div>"
    };
    var page1 = {
        name: 'page1',
        url: '/page1',
        parent: 'widgets',
        resolve: {
            widgets: function (geWidgetService) {
                return geWidgetService.getWidgets('page1');
            }
        },
        views: {
            'content@widgets': {
                template: '<grid grid-info="$resolve.widgets"></grid>'
            }
        }
    };
    $stateProvider.state(widgetsState);
    $stateProvider.state(mainState);
    $stateProvider.state(page1);
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var WidgetService = (function () {
    function WidgetService($q) {
        this.$q = $q;
        this.widgets = {
            page1: {
                rows: 2,
                cols: 2,
                widgets: [
                    {
                        component: 'widget1',
                        row: 0,
                        col: 0
                    },
                    {
                        component: 'widget2',
                        row: 0,
                        col: 1
                    },
                    {
                        component: 'widget3',
                        row: 1,
                        col: 0
                    },
                    {
                        component: 'widget4',
                        row: 1,
                        col: 1
                    }
                ]
            }
        };
    }
    WidgetService.prototype.getWidgets = function (name) {
        var deferred = this.$q.defer();
        deferred.resolve(this.widgets[name]);
        return deferred.promise;
    };
    return WidgetService;
}());
WidgetService.$inject = ['$q'];
exports.WidgetService = WidgetService;
exports.widgetServiceName = 'geWidgetService';


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var widgetsService_1 = __webpack_require__(1);
var states_1 = __webpack_require__(0);
var Grid = __webpack_require__(3);
var WidgetBase = __webpack_require__(4);
var Widget1 = __webpack_require__(5);
var Widget2 = __webpack_require__(6);
var Widget3 = __webpack_require__(7);
var Widget4 = __webpack_require__(8);
var app = angular.module('app', ['ui.router', 'angular-bind-html-compile'])
    .service(widgetsService_1.widgetServiceName, widgetsService_1.WidgetService)
    .component(Grid.name, Grid.component)
    .component(WidgetBase.name, WidgetBase.component)
    .component(Widget1.name, Widget1.component)
    .component(Widget2.name, Widget2.component)
    .component(Widget3.name, Widget3.component)
    .component(Widget4.name, Widget4.component)
    .config(states_1.default)
    .name;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var Controller = (function () {
    function Controller() {
    }
    return Controller;
}());
exports.name = 'grid';
exports.component = {
    template: "\n    <div ng-repeat=\"widget in $ctrl.gridInfo.widgets\">\n        <widget-base widget-config=\"widget\"></widget-base>\n    </div>\n    ",
    controller: Controller,
    bindings: {
        gridInfo: '<'
    }
};


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.name = 'widgetBase';
var WidgetBaseController = (function () {
    function WidgetBaseController() {
    }
    WidgetBaseController.prototype.getHtml = function () {
        return "<" + this.widgetConfig.component + "></" + this.widgetConfig.component + ">";
    };
    WidgetBaseController.prototype.getStyle = function () {
        return {
            left: 1920 / 2 * this.widgetConfig.col + 'px',
            top: 1080 / 2 * this.widgetConfig.row + 'px',
            width: 1920 / 2,
            height: 1080 / 2,
        };
    };
    return WidgetBaseController;
}());
exports.component = {
    template: "\n    <div ng-style=\"$ctrl.getStyle()\" class=\"widget\" bind-html-compile=\"$ctrl.getHtml()\"></div>\n    ",
    controller: WidgetBaseController,
    bindings: {
        'widgetConfig': '<'
    }
};


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.name = 'widget1';
var Controller = (function () {
    function Controller() {
    }
    return Controller;
}());
exports.component = {
    template: 'widget 1',
    controller: Controller,
    require: {
        widgetBase: '^^widgetBase'
    }
};


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.name = 'widget2';
var Controller = (function () {
    function Controller() {
    }
    return Controller;
}());
exports.component = {
    template: 'widget 2',
    controller: Controller,
    require: {
        widgetBase: '^^widgetBase'
    }
};


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.name = 'widget3';
var Controller = (function () {
    function Controller() {
    }
    return Controller;
}());
exports.component = {
    template: 'widget 3',
    controller: Controller,
    require: {
        widgetBase: '^^widgetBase'
    }
};


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.name = 'widget4';
var Controller = (function () {
    function Controller() {
    }
    return Controller;
}());
exports.component = {
    template: 'widget 4',
    controller: Controller,
    require: {
        widgetBase: '^^widgetBase'
    }
};


/***/ })
/******/ ]);