export const name = 'widgetBase';
class WidgetBaseController{
    widgetConfig;
    getHtml(){
        return `<${this.widgetConfig.component}></${this.widgetConfig.component}>`;
    }
    getStyle(){
        return {
            left: 1920 / 2 * this.widgetConfig.col+ 'px',
            top: 1080 / 2 * this.widgetConfig.row + 'px',
            width: 1920 / 2,
            height: 1080 / 2,
        };
    }
}
export const component: ng.IComponentOptions = {
    template: `
    <div ng-style="$ctrl.getStyle()" class="widget" bind-html-compile="$ctrl.getHtml()"></div>
    `,
    controller: WidgetBaseController,
    bindings: {
        'widgetConfig': '<'
    }
};
