interface Widget{
    component: string;
}
interface GridInfo{
    widgets: Widget[];
    rows: any;
    cols: any;
}
class Controller{
    gridInfo: GridInfo;
}
export const name = 'grid';
export const component: ng.IComponentOptions = {
    template: `
    <div ng-repeat="widget in $ctrl.gridInfo.widgets">
        <widget-base widget-config="widget"></widget-base>
    </div>
    `,
    controller: Controller,
    bindings: {
        gridInfo: '<'
    }
};
